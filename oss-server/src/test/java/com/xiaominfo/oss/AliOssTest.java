package com.xiaominfo.oss;

import java.util.List;

import org.junit.Test;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.BucketInfo;

public class AliOssTest {
	
	@Test
	public void testOssClient(){
		// Endpoint以杭州为例，其它Region请按实际情况填写。
		String endpoint = "http://oss-cn-shenzhen.aliyuncs.com";
		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
		String accessKeyId = "LTAIIbga1wmqi8PL";
		String accessKeySecret = "w5yHTzWrQmENz5k5S0iy3HLNk55adH";

		// 创建OSSClient实例。
		OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

		// 列举存储空间。
		List<Bucket> buckets = ossClient.listBuckets();
		for (Bucket bucket : buckets) {
		    System.out.println(" - " + bucket.getName());
		}
		
		// 存储空间的信息包括地域（Region或Location）、创建日期（CreationDate）、拥有者（Owner）、权限（Grants）等。
		BucketInfo info = ossClient.getBucketInfo("hlfs01");
		// 获取地域。
		info.getBucket().getLocation();
		// 获取创建日期。
		info.getBucket().getCreationDate();
		// 获取拥有者信息。
		info.getBucket().getOwner();
		// 获取权限信息。
		info.getGrants();
		// 获取容灾类型。
//		info.getDataRedundancyType();

		// 关闭OSSClient。
		ossClient.shutdown(); 
	}
}
