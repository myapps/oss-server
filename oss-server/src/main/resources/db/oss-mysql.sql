CREATE TABLE IF NOT EXISTS `oss_sys_user` (
  `id` varchar(50) NOT NULL,
  `username` varchar(100),
  `password` varchar(100),
  `create_time` varchar(100),
  `intro` varchar(100),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `oss_information` (
  `id` varchar(50) NOT NULL,
  `root` varchar(100),
  `invoking_root` varchar(100),
  `nginx_log_path` varchar(100),
  `create_time` varchar(100),
  `modified_time` varchar(100),

  PRIMARY KEY (`id`)
);

INSERT INTO oss_information VALUES('5b8b7a2d1aa4414f95a2338aba9571d7','/home/material/','http://192.168.0.7/','/usr/local/nginx/logs/access.log','2018-06-17 10:41:41','2018-06-17 10:45:31');

CREATE TABLE IF NOT EXISTS `oss_material_info` (
  `id` varchar(50) NOT NULL,
  `original_name` varchar(100),
  `store_path` varchar(100),
  `url` varchar(100),
  `user_id` varchar(50),
  `type` varchar(100),
  `byte_str` varchar(100),
  `len` int,
  `create_time` varchar(100),
  `last_modified_time` varchar(100) NOT NULL,
  `app_id` varchar(50),
  `from_ip` varchar(100),

  PRIMARY KEY (`id`, `last_modified_time`)
);

CREATE TABLE IF NOT EXISTS `oss_developer` (

  `id` varchar(50) NOT NULL,
  `name` varchar(100),
  `appid` varchar(50),
  `appsecret` varchar(50),
  `email` varchar(50),
  `create_time` varchar(100),
  `tel` varchar(50),
  `intro` varchar(100),
  `status` varchar(50),
  `use_space` int,
  `use_space_str` varchar(100),

  PRIMARY KEY (`id`)

);
CREATE TABLE IF NOT EXISTS `oss_app_info` (

  `id` varchar(50) NOT NULL,
  `name` varchar(100),
  `code` varchar(50),
  `create_time` varchar(100),
  `intro` varchar(100),
  `dev_id` varchar(50),
  `use_space` int,
  `use_space_str` varchar(100),

  PRIMARY KEY (`id`)

);
CREATE TABLE IF NOT EXISTS `oss_statistic_day` (

  `id` varchar(50) NOT NULL,
  `create_time` varchar(100),
  `files` int,
  `use_spaces` int,
  `cur_date` varchar(100),
  `modified_time` varchar(100),
  `user_id` varchar(50),

  PRIMARY KEY (`id`)
);