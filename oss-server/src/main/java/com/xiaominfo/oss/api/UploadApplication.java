/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.xiaominfo.oss.api;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xiaominfo.oss.common.annotation.NotLogin;
import com.xiaominfo.oss.common.conf.StorageProperties;
import com.xiaominfo.oss.common.pojo.RestfulMessage;
import com.xiaominfo.oss.domain.FileBinaryResponse;
import com.xiaominfo.oss.exception.AssemblerException;
import com.xiaominfo.oss.exception.ErrorCable;
import com.xiaominfo.oss.exception.ErrorConstant;
import com.xiaominfo.oss.module.model.OSSAppInfo;
import com.xiaominfo.oss.module.model.OSSDeveloper;
import com.xiaominfo.oss.module.model.OSSInformation;
import com.xiaominfo.oss.service.ExcelService;
import com.xiaominfo.oss.service.MaterialService;
import com.xiaominfo.oss.service.OSSAppInfoService;
import com.xiaominfo.oss.service.OSSDeveloperService;
import com.xiaominfo.oss.service.OSSInformationService;
import com.xiaominfo.oss.service.OSSMaterialInfoService;

import cn.hutool.core.util.StrUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

/***
 * 上传接口对外api重构
 * @since:oss-server 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2018/06/20 19:47
 */
@RestController
@RequestMapping("/v1/oss")
public class UploadApplication extends RootApis{
	
	private Log log = LogFactory.get();
	
	@Autowired
    MaterialService materialService;

    @Autowired
    OSSMaterialInfoService ossMaterialInfoService;

    @Autowired
    OSSDeveloperService ossDeveloperService;

    @Autowired
    OSSAppInfoService ossAppInfoService;

    @Autowired
    OSSInformationService ossInformationService;

    @Autowired
    ExcelService excelService;
    
    @Autowired
    StorageProperties prop;
	
	/***
     * 提供给前端的统一接口
     * 
     * @return
     */
    @NotLogin
    @PostMapping("/uploadByForm")
    public RestfulMessage uploadMaterialNonProUrl(@RequestParam(value = "project", required = false) String project,
                                                  @RequestParam(value = "module", required = false) String module,
                                                  @RequestParam(value = "appid", required = false) String appid,
                                                  @RequestParam(value = "appsecret", required = false) String appsecret, @RequestParam(value = "file") MultipartFile[] files) {
        RestfulMessage restfulMessage = new RestfulMessage();
        try {
            restfulMessage = uploadFileByForm(project, appid, appsecret, module, files);
        } catch (Exception e) {
            restfulMessage = wrapperException(e);
        }
        return restfulMessage;
    }


    private RestfulMessage uploadFileByForm(String project, String appid, String appsecret, String module, MultipartFile[] files) throws IOException {
        RestfulMessage restfulMessage = new RestfulMessage();
        assertArgumentNotEmpty(project, "project can't be empty!!!");
        assertArgumentNotEmpty(appid, "appid can't be empty ");
        assertArgumentNotEmpty(appsecret, "appsecret can't be empty ");
        OSSDeveloper ossDeveloper = ossDeveloperService.queryByAppid(appid, appsecret);
        assertArgumentNotEmpty(ossDeveloper, "appid or appsecret is invalid");
        //判断文件夹code是否相等
        List<OSSAppInfo> ossAppInfos = ossAppInfoService.queryByDevIds(ossDeveloper.getId());
        if (ossAppInfos == null || ossAppInfos.size() == 0) {
            throw new AssemblerException(new ErrorCable(ErrorConstant.REQUEST_PARAMS_NOT_VALID, "You do not have permission to upload files"));
        }
        boolean flag = false;
        OSSAppInfo ossApp = null;
        for (OSSAppInfo ossAppInfo : ossAppInfos) {
            if (StrUtil.equalsIgnoreCase(ossAppInfo.getCode(), project)) {
                flag = true;
                ossApp = ossAppInfo;
                break;
            }
        }
        if (!flag) {
            throw new AssemblerException(new ErrorCable(ErrorConstant.REQUEST_PARAMS_NOT_VALID, "You do not have permission to upload files"));
        }
        validateProjectName(project);
        OSSInformation ossInformation = ossInformationService.queryOne();
        String root = ossInformation.getRoot();
        //验证文件夹规则,不能包含特殊字符
        StringBuffer path = new StringBuffer();
        path.append(getProjectRootPath(root, prop.getEnv(), project));
        if (StrUtil.isNotEmpty(module)) {
            if (!module.startsWith("/")) {
                path.append("/");
            }
            path.append(module);
        }
        log.info("path:{}", path);
        File projectFile = new File(path.toString());
        if (!projectFile.exists()) {
        	throw new AssemblerException(new ErrorCable(ErrorConstant.AUTHENTICATION_FAILED,"You do not have operating authority for this directory "+project+", or the directory was not created"));
        }
        createDirectoryQuietly(projectFile);
        List<FileBinaryResponse> fileBinaryResponseList = materialService.saveAndStore(ossInformation, ossDeveloper, ossApp, projectFile, files);
        restfulMessage.setData(fileBinaryResponseList);
        successResultCode(restfulMessage);
        return restfulMessage;
    }
}
