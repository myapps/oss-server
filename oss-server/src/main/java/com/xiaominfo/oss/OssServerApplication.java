package com.xiaominfo.oss;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xiaominfo.oss.common.SpringContextUtil;
import com.xiaominfo.oss.common.conf.StorageProperties;
import com.xiaominfo.oss.domain.StoreSource;
import com.xiaominfo.oss.service.impl.AliService;
import com.xiaominfo.oss.sync.netty.MasterServer;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = {"com.xiaominfo.oss"})
@EnableConfigurationProperties(StorageProperties.class)
@PropertySource("classpath:storage.properties")
public class OssServerApplication {

    private final static Log log = LogFactory.get();

	@Autowired
	private StorageProperties prop;
	
	@Autowired
	AliService aliService;


    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(OssServerApplication.class, args);

        //Sync.startMaster();
        Environment env = application.getEnvironment();
        log.info("\n--------------------------------------------------------------------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}\n\t" +
                        "External: \thttp://{}:{}\n--------------------------------------------------------------------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));
        startMaster();
    }

    public static void startMaster() {
        ((Runnable) () -> {
            try {
                String i = SpringContextUtil.getProperty("material.masterPort");
                int masterPort = Integer.parseInt(i);
                new MasterServer().bind(masterPort);
            } catch (Exception e) {
               log.error("初始化异常", e);
            }
        }).run();
    }
    
    
    @Bean
    CommandLineRunner init() {
        return (args) -> {
            registerStoreSource();
        };
    }
	
	public void registerStoreSource() {
//		if (prop.isToqiniu()){
//			StoreSource.registerListensers(qiniuService);
//		}
		
		if (prop.isToalioss()){
			StoreSource.registerListensers(aliService);
		}
		
//		if (prop.isTofastdfs()){
//			StoreSource.registerListensers(fastdfsServcice);
//		}
		
//		if (prop.isTomongodb()){
//			StoreSource.registerListensers(mongoService);
//		}		
		
//		if (prop.isToseaweedfs()){
//			StoreSource.registerListensers(seaweedfsService);
//		}
	}
}
