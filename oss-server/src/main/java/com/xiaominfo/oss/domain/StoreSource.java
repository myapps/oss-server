package com.xiaominfo.oss.domain;

import java.util.ArrayList;
import java.util.List;

import com.xiaominfo.oss.service.FileListener;

public class StoreSource {
	static List<FileListener> listensers = new ArrayList<FileListener>();
	
    public static List<FileListener> getListensers() {
		return listensers;
	}

	public static void registerListensers(FileListener fl){
    	listensers.add(fl);
    }

}
