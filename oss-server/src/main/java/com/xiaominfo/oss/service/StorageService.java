package com.xiaominfo.oss.service;

import com.xiaominfo.oss.domain.OssUploadFile;
import com.xiaominfo.oss.domain.OssUploadResult;

public interface StorageService {
	
	/**
	 * 保存到云端
	 * 
	 * @param ufe
	 * @return
	 */
	OssUploadResult store(OssUploadFile ufe);
	
	/**
	 * 异步保存到云端
	 * 
	 * @param ufe
	 */
	void storeAsync(OssUploadFile ufe);
}
