package com.xiaominfo.oss.service;

import com.xiaominfo.oss.domain.OssUploadFile;
import com.xiaominfo.oss.domain.OssUploadResult;

public interface FileListener {
	
	OssUploadResult store(OssUploadFile ufe);
	
	void download(String fileKeyorName);
	
}
