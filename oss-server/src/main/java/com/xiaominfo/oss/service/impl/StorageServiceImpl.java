package com.xiaominfo.oss.service.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.stereotype.Service;

import com.xiaominfo.oss.domain.StoreSource;
import com.xiaominfo.oss.domain.OssUploadFile;
import com.xiaominfo.oss.domain.OssUploadResult;
import com.xiaominfo.oss.service.FileListener;
import com.xiaominfo.oss.service.StorageService;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

@Service
public class StorageServiceImpl implements StorageService {
	
	 private Log log = LogFactory.get();
	
	ExecutorService executorService = Executors.newFixedThreadPool(5);
	
	/**
	 * 异步保存
	 */
	@Override
	public void storeAsync(OssUploadFile ufe) {
		//第三方存储
		try {
			if (ufe != null) {
				for (FileListener fl : StoreSource.getListensers()) {
										
					executorService.execute(new Runnable() {
						@Override
						public void run() {
							OssUploadResult result = fl.store(ufe);
							log.info("oss location: " + result.location);
						}
					});
				}
			}
		} catch (Exception e1) {
			log.error("文件上传异常", e1);
		}
	}
	
	@Override
	public OssUploadResult store(OssUploadFile ufe) {
		for (FileListener fl : StoreSource.getListensers()) {
			return fl.store(ufe);
		}
		
		return new OssUploadResult();
	}
}
