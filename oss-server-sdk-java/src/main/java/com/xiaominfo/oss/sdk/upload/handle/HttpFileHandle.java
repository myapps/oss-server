package com.xiaominfo.oss.sdk.upload.handle;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaominfo.oss.sdk.OSSClientProperty;
import com.xiaominfo.oss.sdk.client.FileBytesRequest;
import com.xiaominfo.oss.sdk.client.FileBytesResponse;
import com.xiaominfo.oss.sdk.common.OSSClientMessage;
import com.xiaominfo.oss.sdk.upload.IUpload;

public class HttpFileHandle implements IUpload {
	private static final String v1_upload_file_binary_api = "/oss/material/uploadByBinary";
	
	private static final String v1_relate_with_biz_no_api = "/oss/material/relateWithBizNo";

	public OSSClientProperty ossClientProperty;

	public HttpFileHandle(OSSClientProperty ossClientProperty) {
		this.ossClientProperty = ossClientProperty;
	}

	@Override
	public OSSClientMessage<FileBytesResponse> upload(String fileName, InputStream input) {
		OSSClientMessage<FileBytesResponse> ossClientMessage = new OSSClientMessage<>();
		
		// 获取文件原始名称
		String originalName = fileName;
		String mediaType = "unkown";
		int idx = originalName.lastIndexOf(".");
		if (idx > 0) {
			mediaType = originalName.substring(idx + 1);
		}
		
		ByteArrayOutputStream output = null;
		try {
			output = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024 * 4];
			int n = 0;
			
			while (-1 != (n = input.read(buffer))) {
				output.write(buffer, 0, n);
			}
			
			String filebyteString = Base64.encodeBase64String(output.toByteArray());
			FileBytesRequest fileBytesRequest = new FileBytesRequest();
			fileBytesRequest.setFile(filebyteString);
			fileBytesRequest.setMediaType(mediaType);
			fileBytesRequest.setOriginalName(originalName);
			
			return upload(fileBytesRequest);
		} catch (IOException e) {
			handleServerExceptionMessage(ossClientMessage, e);
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (IOException e2) {
				handleServerExceptionMessage(ossClientMessage, e2);
			}
		}
		
		return ossClientMessage;
	}

	/***
	 * 字节字符串形式上传文件
	 * 
	 * @param file
	 * @return
	 */
	@Override
	public OSSClientMessage<FileBytesResponse> upload(File file) {
		OSSClientMessage<FileBytesResponse> ossClientMessage = new OSSClientMessage<>();

		try {
			// 获取文件原始名称
			String originalName = file.getName();
			String mediaType = "unkown";
			int idx = originalName.lastIndexOf(".");
			if (idx > 0) {
				mediaType = originalName.substring(idx + 1);
			}
			String filebyteString = Base64.encodeBase64String(FileUtils.readFileToByteArray(file));
			FileBytesRequest fileBytesRequest = new FileBytesRequest();
			fileBytesRequest.setFile(filebyteString);
			fileBytesRequest.setMediaType(mediaType);
			fileBytesRequest.setOriginalName(originalName);

			return upload(fileBytesRequest);
		} catch (Exception e) {
			handleServerExceptionMessage(ossClientMessage, e);
		}
		return ossClientMessage;
	}

	protected OSSClientMessage<FileBytesResponse> upload(FileBytesRequest fileBytesRequest) {
		return upload(Arrays.asList(new FileBytesRequest[] { fileBytesRequest }));
	}

	protected OSSClientMessage<FileBytesResponse> upload(List<FileBytesRequest> fileBytesRequests) {
		OSSClientMessage<FileBytesResponse> ossClientMessage = new OSSClientMessage<>();
		CloseableHttpResponse closeableHttpResponse = null;
		CloseableHttpClient httpClient = null;
		try {
			HttpPost request = new HttpPost(getUrl(ossClientProperty.getRemote(), v1_upload_file_binary_api));
			addDefaultHeader(request);
			httpClient = HttpClients.createDefault();
			addRequestParam(request, fileBytesRequests, ossClientProperty.getProject());
			closeableHttpResponse = httpClient.execute(request);

			if (closeableHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String content = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
				if (content != null && !"".equals(content)) {
					JSONObject jsonObject = JSONObject.parseObject(content);
					String code = jsonObject.get("code").toString();
					String message = jsonObject.get("message").toString();
					String data = jsonObject.get("data") + "";
					ossClientMessage.setCode(code);
					ossClientMessage.setMessage(message);

					List<FileBytesResponse> fileBytesResponse = JSONArray.parseArray(data, FileBytesResponse.class);

					if (fileBytesResponse != null && fileBytesResponse.size() > 0) {
						ossClientMessage.setData(fileBytesResponse.get(0));
					}
				}
			}
		} catch (Exception e) {
			handleServerExceptionMessage(ossClientMessage, e);
		}
		return ossClientMessage;
	}

	/***
	 * 设置默认header
	 * 
	 * @param request
	 */
	private void addDefaultHeader(HttpUriRequest request) {
		request.addHeader("Content-Encoding", "gzip");
		request.addHeader("Content-type", "application/json");
	}

	private void handleServerExceptionMessage(OSSClientMessage<?> ossClientMessage, Exception e) {
		ossClientMessage.setCode("8500");
		ossClientMessage.setMessage(e.getMessage());
	}

	private void addRequestParam(HttpPost request, List<FileBytesRequest> fileBytesRequests, String project) {
		JSONObject param = createRequestParams(fileBytesRequests, ossClientProperty.getProject());
		request.setEntity(new StringEntity(param.toString(), "UTF-8"));
	}

	/**
	 * 创建请求参数
	 *
	 * @param fileBytesRequests
	 * @param project
	 * @return
	 */
	private JSONObject createRequestParams(List<FileBytesRequest> fileBytesRequests, String project) {
		JSONObject param = new JSONObject();
		param.put("project", project);
		param.put("appid", ossClientProperty.getAppid());
		param.put("appsecret", ossClientProperty.getAppsecret());
		param.put("files", createFileArrs(fileBytesRequests));
		return param;
	}

	/***
	 * 创建数组
	 * 
	 * @param fileBytesRequests
	 * @return
	 */
	private JSONArray createFileArrs(List<FileBytesRequest> fileBytesRequests) {
		JSONArray jsonArray = new JSONArray();
		for (FileBytesRequest fileBytesRequest : fileBytesRequests) {
			JSONObject fileObj = new JSONObject();
			fileObj.put("media_type", fileBytesRequest.getMediaType());
			fileObj.put("file", fileBytesRequest.getFile());
			fileObj.put("original_name", fileBytesRequest.getOriginalName());
			jsonArray.add(fileObj);
		}
		return jsonArray;
	}

	/**
	 * 根据业务单号下载
	 * 
	 * @param bizNo
	 * @return
	 */
	@Override
	public OSSClientMessage<FileBytesResponse> findByBizNo(String bizNo) {
		OSSClientMessage<FileBytesResponse> ossClientMessage = new OSSClientMessage<>();

		return ossClientMessage;
	}

	/**
	 * 根据文件资源ID下载
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public OSSClientMessage<FileBytesResponse> findById(String id) {
		OSSClientMessage<FileBytesResponse> ossClientMessage = new OSSClientMessage<>();

		return ossClientMessage;
	}

	@Override
	public OSSClientMessage<List<FileBytesResponse>> downloadByBizNo(String bizNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OSSClientMessage<FileBytesResponse> downloadById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public OSSClientMessage<Boolean> relateWithBizNo(String id, String bizNo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 关联业务单号
	 */
	@Override
	public OSSClientMessage<Boolean> relateWithBizNo(String[] ids, String bizNo) {
		OSSClientMessage<Boolean> ossClientMessage = new OSSClientMessage<>();
		// TODO Auto-generated method stub
		return ossClientMessage;
	}

	private String getUrl(String host, String api) {
		StringBuffer endpoint = new StringBuffer();
		endpoint.append(host);
		if (api != null && api.trim().length() > 0) {
			if (host.endsWith("/")) {
				endpoint.append(api.substring(1));
			} else {
				endpoint.append(api);
			}
		}

		return endpoint.toString();
	}
}
