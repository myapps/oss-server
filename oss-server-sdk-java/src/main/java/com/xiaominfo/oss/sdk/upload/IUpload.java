package com.xiaominfo.oss.sdk.upload;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.xiaominfo.oss.sdk.client.FileBytesResponse;
import com.xiaominfo.oss.sdk.common.OSSClientMessage;

public interface IUpload {

	/**
	 * 文件上传
	 * 
	 * @param fileName
	 * @param input
	 * @return
	 */
	default OSSClientMessage<FileBytesResponse> upload(String fileName, InputStream input) {
		OSSClientMessage<FileBytesResponse> rtn = new OSSClientMessage<>();
		return rtn;
	}
	
    /**
     * 文件上传
     *
     * @return
     */
	default OSSClientMessage<FileBytesResponse> upload(File file) {
		OSSClientMessage<FileBytesResponse> rtn = new OSSClientMessage<>();
		return rtn;
	}

	default OSSClientMessage<FileBytesResponse> findById(String id) {
		OSSClientMessage<FileBytesResponse> rtn = new OSSClientMessage<>();
		return rtn;
	}

	default OSSClientMessage<FileBytesResponse> findByBizNo(String bizNo) {
		OSSClientMessage<FileBytesResponse> rtn = new OSSClientMessage<>();
		return rtn;
	}

	/**
	 * 根据业务单号下载关联的所有文件，为列表，业务单号可以关联多个文件
	 * 
	 * @param bizNo
	 * @return
	 */
	default OSSClientMessage<List<FileBytesResponse>> downloadByBizNo(String bizNo) {
		OSSClientMessage<List<FileBytesResponse>> rtn = new OSSClientMessage<>();
		return rtn;
	}
	
	/**
	 * 根据ID下载关联的所有文件
	 * 
	 * @param id
	 * @return
	 */
	default OSSClientMessage<FileBytesResponse> downloadById(String id) {
		OSSClientMessage<FileBytesResponse> rtn = new OSSClientMessage<>();
		
		return rtn;
	}
	
	/**
	 * 关联业务单号
	 * 
	 * @param id
	 * @param bizNo
	 * @return
	 */
	default OSSClientMessage<Boolean> relateWithBizNo(String id, String bizNo) {
		OSSClientMessage<Boolean> rtn = new OSSClientMessage<>();
		return rtn;
	}
	
	/**
	 * 关联业务单号，允许多个文件关联同一个业务单
	 * 
	 * @param id
	 * @param bizNo
	 * @return
	 */
	default OSSClientMessage<Boolean> relateWithBizNo(String[] ids, String bizNo) {
		OSSClientMessage<Boolean> rtn = new OSSClientMessage<>();
		return rtn;
	}
}
